# Authors

net-color spec (first implementation): 
                Tomas Carnecky

XcmEdidParse, XcmEvents, XcmDDC maintainance:
                Kai-Uwe Behrmann <ku.b@gmx.de>

XcmEdidParse (some decoding):
                Soren Sandmann <sandmann@redhat.com>

